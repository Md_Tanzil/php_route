<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        return view('welcome');
    }


    public function productTable()
    {
        return view('productTable');
    }


    public function productDetails($id)
    {
        return view('productDetails', ['id' => $id]);
    }
}